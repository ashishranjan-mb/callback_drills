const fs=require('fs')
const path=require('path')

function createDirectory(){
    return new Promise((resolve,reject) => {
        const directoryPath = './Ashish_directory';

        fs.mkdir(directoryPath, { recursive:true }, (err) => {

            if(err){

                reject(err);
            }

            else{

                console.log(`Directory "./Ashish_directory" created`);
                resolve(directoryPath);
            }
        });
    });
}

function createJsonFiles(fileName,data){
    return new Promise((resolve,reject)=>{
        const files = {"fileName":data}.toString();
        fs.writeFile(fileName,files,"utf-8",(err)=>{
            if(err){
                reject(err)
            }
            else{
                console.log(`File "${fileName}" created`)
                resolve(fileName)
            }
        });
    });
} 

function deleteJsonFiles(fileName){
    return new Promise((resolve,reject) => {

        fs.unlink(fileName,(err) => {

            if(err){

                reject(err);
            }

            else{

                console.log(`File "${fileName}" deleted`);
                resolve();
            }
        })
    })
}

module.exports = {createDirectory, createJsonFiles, deleteJsonFiles}
