const fs = require('fs');
const path = require('path');
function callback(err){
    if(err)
        console.log(err)
    console.log("File Created");
}

function converttoUpper(read,write,filepath){
    fs.readFile(read,function(err,data){
        if(err){
            console.log(err)
        }
        else{
            console.log("File Reading")
            const convertUpper = data.toString().toUpperCase()

            fs.writeFile(write, convertUpper, "utf-8", () => {
                console.log("Converted in UPPERCASE LETTER")
            })
            
            
                fs.appendFile(filepath, "\n"+write, "utf-8", ()=>{
                    console.log("Stored in a new file upper")
                })
                
            
            
        }
    })
}

function converttoLowerAndSplit(read,write,filepath){
    fs.readFile(read,(err,data)=>{
        if(err)
            console.log(err)
        else{
        console.log("File Reading")
        const convertLower = data.toString().toLowerCase().split(".").join(".\n")
        fs.writeFile(write,convertLower,"utf-8",() => {
            console.log("Converted in lowercase letter")
        })

        const splitInToSentence = convertLower.toString()
        fs.writeFile(write,splitInToSentence,"utf-8",()=>{
            fs.appendFile(filepath, "\n"+write, "utf-8", callback)
        })
        }
    })
}
//console.log(converttoLowerAndSplit("/home/ashish/Desktop/Callback_drill/lipsumUpper.txt","/home/ashish/Desktop/Callback_drill/lipsumLower.txt","/home/ashish/Desktop/Callback_drill/filenames.txt"))

function sortAndStore(read,write){
    fs.readFile(read,"utf-8",(err,data)=>{
        if(err)
            console.log(err)
        else{
            console.log("File Reading")
            const PreviousData = data
            const sortedData = PreviousData.split(".").sort()
            const newdata = sortedData.toString()
            fs.writeFile(write,newdata,"utf-8",()=>{
                console.log("File sorted")
                fs.appendFile('/home/ashish/Desktop/Callback_drill/filenames.txt', "\n"+write, "utf-8", callback)
            })
           
        }
    })
}

function deleteAllFiles(read) {
    fs.readFile(read, "utf-8", (err, data) => {
        if (err) {
            console.log(err)
        } else {
            console.log("File Reading")
            const arr = data.split("\n")

            console.log(arr)
            console.log(typeof (arr))

            function deleteFiles(files, callback) {
                let i = files.length;
                files.map(function (filepath) {
                    fs.unlink(filepath, function (err) {
                        i--;
                        if (err) {
                            callback(err);
                            return;
                        } else if (i <= 0) {
                            callback(null);
                        }
                    });
                });
            }

            deleteFiles(arr, (err) => {
                if (err) {
                    console.log('')
                } else {
                    console.log('all files removed')
                }
            })

        }
    })
}


    

module.exports = {converttoUpper,converttoLowerAndSplit,sortAndStore,deleteAllFiles}
