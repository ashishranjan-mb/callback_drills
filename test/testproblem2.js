const {converttoUpper,converttoLowerAndSplit,sortAndStore,deleteAllFiles} = require("../problem2.js")

function upperconvert(){
    converttoUpper('/home/ashish/Desktop/Callback_drill/lipsum.txt','/home/ashish/Desktop/Callback_drill/lipsumUpper.txt','/home/ashish/Desktop/Callback_drill/filenames.txt')
}
setTimeout(upperconvert,4000)
function convertlower(){
    converttoLowerAndSplit('/home/ashish/Desktop/Callback_drill/lipsumUpper.txt','/home/ashish/Desktop/Callback_drill/lipsumlower.txt','/home/ashish/Desktop/Callback_drill/filenames.txt')
}
setTimeout(convertlower,5000)
function sort_store(){
sortAndStore('/home/ashish/Desktop/Callback_drill/lipsumlower.txt','/home/ashish/Desktop/Callback_drill/sortedlipsumData.txt')
}
setTimeout(sort_store,6000)
function deleteAll(){
    deleteAllFiles("/home/ashish/Desktop/Callback_drill/filenames.txt")
}
setTimeout(deleteAll,8000);
