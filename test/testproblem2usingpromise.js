const {
  converttoUpper,
  converttoLowerAndSplit,
  sortAndStore,
  deleteAllFiles,
} = require("../problem2usingpromise");

const fs = require("fs");


converttoUpper(
  "/home/ashish/Desktop/Callback_drill/lipsum.txt",
  "/home/ashish/Desktop/Callback_drill/lipsumUpper.txt",
  "/home/ashish/Desktop/Callback_drill/filenames.txt"
)
.then((value) => {
    fs.writeFile(value.write, value.data, "utf-8", () => {
      console.log("Converted in UPPERCASE LETTER");
    });

    fs.appendFile(value.filepath, "\n" + value.write, "utf-8", () => {
      console.log("Stored in a new file upper");
    });
  })
  .then(() => {});
converttoLowerAndSplit(
  "/home/ashish/Desktop/Callback_drill/lipsum.txt",
  "/home/ashish/Desktop/Callback_drill/lipsumlower.txt",
  "/home/ashish/Desktop/Callback_drill/filenames.txt"
)
.then((value) => {
    fs.writeFile(value.write, value.data, "utf-8", () => {
      console.log("Converted in lowercase letter");
    });

    fs.writeFile(value.write, value.data, "utf-8", () => {
      fs.appendFile(value.filepath, "\n" + value.write, "utf-8", callback => {
        console.log("Stored in filenames.txt");
        })
    });
  })
  .then(() => {
    sortAndStore(
      "/home/ashish/Desktop/Callback_drill/lipsumlower.txt",
      "/home/ashish/Desktop/Callback_drill/lipsumsorted.txt",
      "/home/ashish/Desktop/Callback_drill/filenames.txt"
    ).then((value) => {
      fs.writeFile(value.write, value.data, "utf-8", () => {
        console.log("File sorted");
        fs.appendFile(
          "/home/ashish/Desktop/Callback_drill/filenames.txt",
          "\n" + value.write,
          "utf-8",
          callback => {
            console.log("Stored in filenames.txt")
            }
        );
      });
    });
  })
  .then(() => {
    deleteAllFiles("/home/ashish/Desktop/Callback_drill/filenames.txt");
  });
