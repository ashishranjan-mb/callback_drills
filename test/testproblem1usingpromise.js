const {createDirectory,createJsonFiles,deleteJsonFiles} = require("../problem1usingpromise.js")
createDirectory()
createJsonFiles("/home/ashish/Desktop/Callback_drill/Ashish_directory/printFruits.json","Banana")
createJsonFiles("/home/ashish/Desktop/Callback_drill/Ashish_directory/printAnimal.json","Cow")
createJsonFiles("/home/ashish/Desktop/Callback_drill/Ashish_directory/printName.json","Ashish")
createJsonFiles("/home/ashish/Desktop/Callback_drill/Ashish_directory/printDate.json","01-04-2022")
createJsonFiles("/home/ashish/Desktop/Callback_drill/Ashish_directory/printMonth.json","April")

let files = ['./Ashish_directory/printAnimal.json', './Ashish_directory/printDate.json', './Ashish_directory/printFruits.json', 
'./Ashish_directory/printMonth.json', './Ashish_directory/printName.json'];

setTimeout(function() {
    files.forEach((file)=>{
        deleteJsonFiles(file)
    })  
  },9000)