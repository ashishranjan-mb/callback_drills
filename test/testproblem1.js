let {createDirectoryOfRandomFiles, createJsonData, deleteJsonFiles} = require("../problem1.js")
createDirectoryOfRandomFiles("Ashish_dir")

createJsonData("./Ashish_dir/printFruits.json","Banana")
createJsonData("./Ashish_dir/printAnimal.json","Cow")
createJsonData("./Ashish_dir/printName.json","Ashish")
createJsonData("./Ashish_dir/printDate.json","01-04-2022")
createJsonData("./Ashish_dir/printMonth.json","April")


let files = ['./Ashish_dir/printAnimal.json', './Ashish_dir/printDate.json', './Ashish_dir/printFruits.json', 
'./Ashish_dir/printMonth.json', './Ashish_dir/printName.json'];

setTimeout(function() {
  deleteJsonFiles(files)
},1000)
