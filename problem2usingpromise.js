const fs = require("fs");
const path = require("path");

function converttoUpper(read, write, filepath) {
  return new Promise((resolve, reject) => {
    fs.readFile(read, function (err, data) {
      if (err) {
        reject(err);
      } else {
        console.log("File Reading");

        const convertUpper = data.toString().toUpperCase();
        const dataToPass = {
          data: convertUpper,
          read: read,
          write: write,
          filepath: filepath,
        };
        resolve(dataToPass);
      }
    });
  });
}

function converttoLowerAndSplit(read, write, filepath) {
  return new Promise((resolve, reject) => {
    fs.readFile(read, (err, data) => {
      if (err) reject(err);
      else {
        console.log("File Reading");
        const convertLower = data
          .toString()
          .toLowerCase()
          .split(".")
          .join(".\n");
        const splitInToSentence = convertLower.toString();
        const dataToPass = {
          data: convertLower,
          read: read,
          write: write,
          filepath: filepath,
        };
        resolve(dataToPass);
      }
    });
  });
}

function sortAndStore(read, write, filepath) {
  return new Promise((resolve, reject) => {
    fs.readFile(read, "utf-8", (err, data) => {
      if (err) reject(err);
      else {
        console.log("File Reading");
        const PreviousData = data;
        const sortedData = PreviousData.split(".").sort();
        const newdata = sortedData.toString();
        const dataToPass = {
          data: newdata,
          read: read,
          write: write,
          filepath: filepath,
        };
        resolve(dataToPass);
      }
    });
  });
}

function deleteAllFiles(read) {
  return new Promise((resolve, reject) => {
    fs.readFile(read, "utf-8", (err, data) => {
      if (err) {
        reject(err);
      } else {
        console.log("File Reading");
        const arr = data.split("\n");

        console.log(arr);
        console.log(typeof arr);

        function deleteFiles(files, callback) {
          let i = files.length;
          files.map(function (filepath) {
            fs.unlink(filepath, function (err) {
              i--;
              if (err) {
                callback(err);
                return;
              } else if (i <= 0) {
                callback(null);
              }
            });
          });
        }

        deleteFiles(arr, (err) => {
          if (err) {
            console.log("");
          } else {
            console.log("all files removed");
          }
        });
      }
    });
  });
}
module.exports = {
  converttoUpper,
  converttoLowerAndSplit,
  sortAndStore,
  deleteAllFiles,
};
