const fs = require('fs')
let path = require("path")
function createDirectoryOfRandomFiles(dir){
    let createPath = path.join(__dirname, dir)
    fs.mkdir(createPath, function(err) {
        if (err) {
            console.log(err)
        }
        console.log("Directory created")
    })
    
}
function createJsonData(path,data){
    fs.writeFile(path,data,"utf-8",function(err){
        if(err){
            console.log(err)
        }else{
            console.log('File created')
        }
    })
}
function deleteJsonFiles(files){
    let i = files.length;
    files.forEach(function(filepath){
      fs.unlink(filepath, function(err) {
        i--;
        console.log("All Files deleted")
      });
    });
  }

module.exports = {createDirectoryOfRandomFiles,createJsonData,deleteJsonFiles};
